from decimal import Decimal
from tiingo import TiingoClient
from common import get_properties

tiingo_properties = get_properties()['tiingo']
tiingo_client_config = {
    'api_key': tiingo_properties['token'],
    'session': True
}
tiingo_client = TiingoClient(tiingo_client_config)

# Consider caching to disk
tiingo_cache = {}


def get_current_price(symbol: str) -> Decimal:
    cached_value = tiingo_cache.get(symbol)

    if cached_value is not None:
        return cached_value

    latest_prices = tiingo_client.get_ticker_price(symbol, frequency="daily")
    latest_price = Decimal(latest_prices[0]['adjClose'])
    tiingo_cache[symbol] = latest_price

    return latest_price


