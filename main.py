import os
import sys
from pathlib import Path

from common import HOME_DIR_NAME, HOME_DIR_PATH, get_properties
from google_sheets import GoogleSheets
from imports.importer import import_events
from projections.asset import project as asset_projection


def init_home_dir():
    if HOME_DIR_NAME in os.listdir(Path.home()):
        return

    os.mkdir(HOME_DIR_PATH)
    print("Created data dir in home")
    sys.exit()


def init():
    init_home_dir()


if __name__ == '__main__':
    init()

    properties = get_properties()

    gsheets = GoogleSheets(HOME_DIR_PATH, properties['google_sheets'])

    events = import_events()

    for e in events:
        print(e)

    asset_projection(gsheets, events)
