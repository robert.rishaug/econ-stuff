from pathlib import Path

import yaml

HOME_DIR_NAME = ".econ-stuff"
HOME_DIR_PATH = Path.joinpath(Path.home(), HOME_DIR_NAME)
PROPERTIES_FILE_NAME = "properties.yml"


def get_properties(section: str = None) -> dict:

    properties_file_path = Path.joinpath(HOME_DIR_PATH, PROPERTIES_FILE_NAME)
    properties_file = open(properties_file_path, 'r')

    properties = yaml.load(properties_file, Loader=yaml.FullLoader)

    if section is not None:
        return properties[section]
    else:
        return properties
