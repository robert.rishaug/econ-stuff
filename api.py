import datetime
from dataclasses import dataclass
from decimal import Decimal
from enum import Enum


@dataclass(frozen=True)
class AssetAggregate:
    symbol: str
    amount: Decimal = Decimal(0)
    total_price: Decimal = Decimal(0) # better names? Should separate cost from value (cost = initial value + fees?)
    currency: str = ''


class EventType(str, Enum):
    BUY = 'BUY'
    SELL = 'SELL'
    SPLIT = 'SPLIT'
    RENAME = 'RENAME'


# Could merge and Buy and Sell events to Transaction event?
@dataclass(frozen=True)
class BuyEvent:
    sequence_num: int
    symbol: str
    amount: Decimal
    price: Decimal
    currency: str
    fee: Decimal
    datetime: datetime
    type: EventType = EventType.BUY


@dataclass(frozen=True)
class SellEvent:
    sequence_num: int
    symbol: str
    amount: Decimal
    price: Decimal
    currency: str
    fee: Decimal
    datetime: datetime
    type: EventType = EventType.SELL


@dataclass(frozen=True)
class SplitEvent:
    symbol: str
    factor: Decimal
    datetime: datetime
    sequence_num: int = 0
    type: EventType = EventType.SPLIT


@dataclass(frozen=True)
class RenameEvent:
    symbol: str
    new_symbol: str
    datetime: datetime
    sequence_num: int = 0
    type: EventType = EventType.RENAME
