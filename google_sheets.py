# Create google cloud project
# Active Google Sheets API
# Create service account
# Share sheet with service account, use "email"
# Create key for service account and download as .json file

from pathlib import Path

from google.oauth2 import service_account
from googleapiclient.discovery import build

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


class GoogleSheets:
    
    def __init__(self, path, properties):
        self.config_dir_path: Path = path
        self.spreadsheet_id = properties['spreadsheet_id']

        api_key_path = Path.joinpath(self.config_dir_path, 'gsapikey.json')
        credentials = service_account.Credentials.from_service_account_file(
            str(api_key_path), 
            scopes=SCOPES
        )
        service = build('sheets', 'v4', credentials=credentials)
        self.sheet = service.spreadsheets()

    def write(self, values, range, input_option="USER_ENTERED"):
        request = self.sheet.values().update(
            spreadsheetId=self.spreadsheet_id,
            range=range,
            valueInputOption=input_option,
            body={"values": values}
        ).execute()
        print(request)


def get_nok_to_usd_formula() -> str:
    return '=GOOGLEFINANCE("Currency:USDNOK")'


def get_asset_price_formula(symbol: str) -> str:
    return f'=GOOGLEFINANCE("{symbol}"; "price")'
