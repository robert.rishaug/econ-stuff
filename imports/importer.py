from .nordnet import import_nordnet_csvs
from .split import import_split_events
from .rename import import_rename_events

import_funcs = [
    import_nordnet_csvs,
    import_split_events,
    import_rename_events
]


def import_events():
    # events = [y for ys in import_funcs for y in ys()] ¯\_(ツ)_/¯
    events = []

    for func in import_funcs:
        events.extend(func())

    events = sorted(events, key=lambda x: x.sequence_num)
    events = sorted(events, key=lambda x: x.datetime)
    return events
