from datetime import datetime
from decimal import Decimal

from api import SplitEvent


def import_split_events():
    return [
        SplitEvent('TSLA', Decimal(5), datetime.fromisoformat('2020-08-31T08:00:00'))
    ]

