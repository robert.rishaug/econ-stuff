import os
from datetime import datetime
from decimal import Decimal
from pathlib import Path

import pandas as pd

from api import BuyEvent, SellEvent, EventType
from common import HOME_DIR_PATH

DATA_PATH = Path.joinpath(HOME_DIR_PATH, "data", "nordnet")


def resolve_event_type(nordnet_event_type: str) -> EventType:
    if nordnet_event_type == 'KJØPT':
        return EventType.BUY
    elif nordnet_event_type == 'SALG':
        return EventType.SELL
    else:
        print(f"Failed to resolve transaction type for [{nordnet_event_type}]")
        raise Exception()


## Could probably be improved
def to_decimal(value):
    return Decimal(str(value).replace(',', '.').replace(' ', ''))


def resolve_price_nok(series: pd.Series):
    amount = int(series['Antall'])
    value = to_decimal(series['Kurs'])
    ex_rate = to_decimal(series['Vekslingskurs'])
    fee = to_decimal(series['Totale Avgifter'])
    return (amount * value * ex_rate) + fee


def resolve_currency(ex_rate: Decimal):
    if ex_rate == 1:
        return 'NOK'
    else:
        return "USD"


def parse_buy_event(series: pd.Series):
    ex_rate = to_decimal(series['Vekslingskurs'])
    currency = resolve_currency(ex_rate)
    fee = to_decimal(series['Totale Avgifter']) / ex_rate

    return BuyEvent(
        int(series['Id']),
        series['Verdipapir'],
        series['Antall'],
        to_decimal(series['Kurs']),
        currency,
        # to_decimal(series['Totale Avgifter']),
        fee,
        datetime.fromisoformat(series['Handelsdag'])
    )


def parse_sell_event(series: pd.Series):
    ex_rate = to_decimal(series['Vekslingskurs'])
    currency = resolve_currency(ex_rate)
    fee = to_decimal(series['Totale Avgifter']) / ex_rate
    
    return SellEvent(
        int(series['Id']),
        series['Verdipapir'],
        series['Antall'],
        to_decimal(series['Kurs']),
        currency,
        # to_decimal(series['Totale Avgifter']),
        fee,
        datetime.fromisoformat(series['Handelsdag'])
    )


def parse_event(series: pd.Series):
    nordnet_event_type = series['Transaksjonstype']
    event_type = resolve_event_type(nordnet_event_type)

    if event_type == EventType.BUY:
        return parse_buy_event(series)
    elif event_type == EventType.SELL:
        return parse_sell_event(series)
    else:
        print(f"Failed to parse event of type [{nordnet_event_type}]")
        raise Exception()


def parse_events(frame: pd.DataFrame):
    return [parse_event(row) for index, row in frame.iterrows()]


def import_nordnet_csv(filename):
    frame = pd.read_csv(
        filename, 
        decimal=',', 
        sep='\t', 
        lineterminator='\n', 
        skiprows=0, 
        encoding='UTF-16 LE'
    )
    return parse_events(frame)


def import_nordnet_csvs():
    csv_file_names = filter(lambda fn: str(fn).endswith(".csv"), os.listdir(DATA_PATH))
    events = []
    for fn in csv_file_names:
        events = events + import_nordnet_csv(Path.joinpath(DATA_PATH, fn))
    return events

