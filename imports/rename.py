from datetime import datetime

from api import RenameEvent


def import_rename_events():
    return [
        RenameEvent('IPOE', 'SOFI', datetime.fromisoformat('2021-06-01T00:00:01'))
    ]
