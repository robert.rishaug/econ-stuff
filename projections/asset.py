from api import AssetAggregate, EventType, BuyEvent, SellEvent, SplitEvent, RenameEvent
from decimal import Decimal
from ext_api import tiingo
from google_sheets import GoogleSheets


def apply_buy_event(aggregate: AssetAggregate, event: BuyEvent):
    return AssetAggregate(
        aggregate.symbol,
        aggregate.amount + event.amount,
        aggregate.total_price + (event.price * event.amount) + event.fee,
        event.currency
    )


def apply_sell_event(aggregate: AssetAggregate, event: SellEvent):
    avg_price = aggregate.total_price / aggregate.amount

    return AssetAggregate(
        aggregate.symbol,
        aggregate.amount - event.amount,
        aggregate.total_price - (avg_price * event.amount),
        event.currency
    )


def apply_split_event(aggregate: AssetAggregate, event: SplitEvent):
    return AssetAggregate(
        aggregate.symbol,
        aggregate.amount * event.factor,
        aggregate.total_price,
        aggregate.currency
    )


def apply_rename_event(aggregate: AssetAggregate, event: RenameEvent):
    return AssetAggregate(
        event.new_symbol,
        aggregate.amount,
        aggregate.total_price,
        aggregate.currency
    )


event_appliers = {
    EventType.BUY: apply_buy_event,
    EventType.SELL: apply_sell_event,
    EventType.SPLIT: apply_split_event,
    EventType.RENAME: apply_rename_event
}


def apply_event(aggregate: AssetAggregate, event):

    if aggregate is None:
        aggregate = AssetAggregate(event.symbol)

    event_applier = event_appliers[event.type]
    return event_applier(aggregate, event)


def aggregate_events(events, prune_empty=True):
    aggregate_by_symbol = {}

    for event in events:
        old_aggregate = aggregate_by_symbol.pop(event.symbol, None)
        new_aggregate = apply_event(old_aggregate, event)
        aggregate_by_symbol[new_aggregate.symbol] = new_aggregate

    if prune_empty:
        aggregate_by_symbol = dict(filter(lambda kv: kv[1].amount != Decimal(0), aggregate_by_symbol.items()))

    return aggregate_by_symbol


def convert_aggregate_to_array(aggregate: AssetAggregate):
    total_price = round(aggregate.total_price, 2)
    avg_price = round(aggregate.total_price / aggregate.amount, 2)
    current_price = round(tiingo.get_current_price(aggregate.symbol), 2)
    current_value = round(aggregate.amount * current_price, 2)
    delta_value = round(current_value - total_price, 2)
    percent_delta = str(round((current_value - total_price) / total_price, 2)).replace('.', '') + '%'

    return [
        str(aggregate.symbol),
        str(aggregate.amount),
        str(total_price),
        aggregate.currency,
        str(avg_price),
        str(current_price),
        str(current_value),
        str(delta_value),
        str(percent_delta)
    ]


def update_gsheets(gsheets: GoogleSheets, aggregate_by_symbol: dict):
    aggregate_array = [['Symbol', 'Amount', 'Total price', 'Currency', 'Avg price', 'Current price', 'Current value', 'Delta', '% delta']]
    aggregate_array = aggregate_array + [convert_aggregate_to_array(v) for k, v in aggregate_by_symbol.items()]
    gsheets.write(aggregate_array, "AssetAggregate")


def project(gsheets: GoogleSheets, events):
    aggregate_by_symbol = aggregate_events(events)
    for symbol, aggregate in aggregate_by_symbol.items():
        if aggregate.amount != Decimal(0):
            print(symbol)
            print(aggregate)

    update_gsheets(gsheets, aggregate_by_symbol)
